﻿namespace _06_WindowsFormsApplicationClases
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_C_D = new System.Windows.Forms.Button();
            this.button_B = new System.Windows.Forms.Button();
            this.button_E = new System.Windows.Forms.Button();
            this.button_A = new System.Windows.Forms.Button();
            this.label_Resultado = new System.Windows.Forms.Label();
            this.button_Sacar = new System.Windows.Forms.Button();
            this.button_G = new System.Windows.Forms.Button();
            this.button_Pares = new System.Windows.Forms.Button();
            this.button_Impares = new System.Windows.Forms.Button();
            this.button_UnDigito = new System.Windows.Forms.Button();
            this.button_DosDigitos = new System.Windows.Forms.Button();
            this.button_estan = new System.Windows.Forms.Button();
            this.button_NoEstan = new System.Windows.Forms.Button();
            this.label_Resultado2 = new System.Windows.Forms.Label();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button_C_D
            // 
            this.button_C_D.Location = new System.Drawing.Point(212, 44);
            this.button_C_D.Name = "button_C_D";
            this.button_C_D.Size = new System.Drawing.Size(94, 36);
            this.button_C_D.TabIndex = 0;
            this.button_C_D.Text = "LIMPIAR";
            this.button_C_D.UseVisualStyleBackColor = true;
            this.button_C_D.Click += new System.EventHandler(this.Limpiar_Click);
            // 
            // button_B
            // 
            this.button_B.Location = new System.Drawing.Point(112, 44);
            this.button_B.Name = "button_B";
            this.button_B.Size = new System.Drawing.Size(94, 36);
            this.button_B.TabIndex = 2;
            this.button_B.Text = "15 aleatorios";
            this.button_B.UseVisualStyleBackColor = true;
            this.button_B.Click += new System.EventHandler(this.B_Click);
            // 
            // button_E
            // 
            this.button_E.Location = new System.Drawing.Point(312, 44);
            this.button_E.Name = "button_E";
            this.button_E.Size = new System.Drawing.Size(94, 36);
            this.button_E.TabIndex = 3;
            this.button_E.Text = "Invertir";
            this.button_E.UseVisualStyleBackColor = true;
            this.button_E.Click += new System.EventHandler(this.E_Click);
            // 
            // button_A
            // 
            this.button_A.Location = new System.Drawing.Point(12, 44);
            this.button_A.Name = "button_A";
            this.button_A.Size = new System.Drawing.Size(94, 36);
            this.button_A.TabIndex = 1;
            this.button_A.Text = "10 aleatorios";
            this.button_A.UseVisualStyleBackColor = true;
            this.button_A.Click += new System.EventHandler(this.A_Click);
            // 
            // label_Resultado
            // 
            this.label_Resultado.AutoSize = true;
            this.label_Resultado.Location = new System.Drawing.Point(45, 208);
            this.label_Resultado.Name = "label_Resultado";
            this.label_Resultado.Size = new System.Drawing.Size(61, 13);
            this.label_Resultado.TabIndex = 4;
            this.label_Resultado.Text = "Resultado: ";
            // 
            // button_Sacar
            // 
            this.button_Sacar.Location = new System.Drawing.Point(12, 86);
            this.button_Sacar.Name = "button_Sacar";
            this.button_Sacar.Size = new System.Drawing.Size(94, 36);
            this.button_Sacar.TabIndex = 5;
            this.button_Sacar.Text = "F";
            this.button_Sacar.UseVisualStyleBackColor = true;
            this.button_Sacar.Click += new System.EventHandler(this.button_Sacar_Click);
            // 
            // button_G
            // 
            this.button_G.Location = new System.Drawing.Point(112, 86);
            this.button_G.Name = "button_G";
            this.button_G.Size = new System.Drawing.Size(94, 36);
            this.button_G.TabIndex = 6;
            this.button_G.Text = "G";
            this.button_G.UseVisualStyleBackColor = true;
            this.button_G.Click += new System.EventHandler(this.button_G_Click);
            // 
            // button_Pares
            // 
            this.button_Pares.Location = new System.Drawing.Point(212, 86);
            this.button_Pares.Name = "button_Pares";
            this.button_Pares.Size = new System.Drawing.Size(94, 36);
            this.button_Pares.TabIndex = 7;
            this.button_Pares.Text = "Pares";
            this.button_Pares.UseVisualStyleBackColor = true;
            this.button_Pares.Click += new System.EventHandler(this.button_Pares_Click);
            // 
            // button_Impares
            // 
            this.button_Impares.Location = new System.Drawing.Point(312, 86);
            this.button_Impares.Name = "button_Impares";
            this.button_Impares.Size = new System.Drawing.Size(94, 36);
            this.button_Impares.TabIndex = 8;
            this.button_Impares.Text = "Impares";
            this.button_Impares.UseVisualStyleBackColor = true;
            this.button_Impares.Click += new System.EventHandler(this.button_Impares_Click);
            // 
            // button_UnDigito
            // 
            this.button_UnDigito.Location = new System.Drawing.Point(12, 128);
            this.button_UnDigito.Name = "button_UnDigito";
            this.button_UnDigito.Size = new System.Drawing.Size(94, 36);
            this.button_UnDigito.TabIndex = 9;
            this.button_UnDigito.Text = "Un Dígito";
            this.button_UnDigito.UseVisualStyleBackColor = true;
            this.button_UnDigito.Click += new System.EventHandler(this.button_UnDigito_Click);
            // 
            // button_DosDigitos
            // 
            this.button_DosDigitos.Location = new System.Drawing.Point(112, 128);
            this.button_DosDigitos.Name = "button_DosDigitos";
            this.button_DosDigitos.Size = new System.Drawing.Size(94, 36);
            this.button_DosDigitos.TabIndex = 10;
            this.button_DosDigitos.Text = "Dos Dígitos";
            this.button_DosDigitos.UseVisualStyleBackColor = true;
            this.button_DosDigitos.Click += new System.EventHandler(this.button_DosDigitos_Click);
            // 
            // button_estan
            // 
            this.button_estan.Location = new System.Drawing.Point(212, 128);
            this.button_estan.Name = "button_estan";
            this.button_estan.Size = new System.Drawing.Size(94, 36);
            this.button_estan.TabIndex = 11;
            this.button_estan.Text = "Están en ambas listas";
            this.button_estan.UseVisualStyleBackColor = true;
            this.button_estan.Click += new System.EventHandler(this.button_estan_Click);
            // 
            // button_NoEstan
            // 
            this.button_NoEstan.Location = new System.Drawing.Point(312, 128);
            this.button_NoEstan.Name = "button_NoEstan";
            this.button_NoEstan.Size = new System.Drawing.Size(94, 36);
            this.button_NoEstan.TabIndex = 12;
            this.button_NoEstan.Text = "No están en ammbas listas";
            this.button_NoEstan.UseVisualStyleBackColor = true;
            this.button_NoEstan.Click += new System.EventHandler(this.button_NoEstan_Click);
            // 
            // label_Resultado2
            // 
            this.label_Resultado2.AutoSize = true;
            this.label_Resultado2.Location = new System.Drawing.Point(45, 237);
            this.label_Resultado2.Name = "label_Resultado2";
            this.label_Resultado2.Size = new System.Drawing.Size(61, 13);
            this.label_Resultado2.TabIndex = 13;
            this.label_Resultado2.Text = "Resultado: ";
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(169, 263);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(94, 36);
            this.button6.TabIndex = 14;
            this.button6.Text = "Rellenar ambas listas";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(325, 209);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(80, 28);
            this.button7.TabIndex = 15;
            this.button7.Text = "button7";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // Form1
            // 
            this.ClientSize = new System.Drawing.Size(463, 311);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.label_Resultado2);
            this.Controls.Add(this.button_NoEstan);
            this.Controls.Add(this.button_estan);
            this.Controls.Add(this.button_DosDigitos);
            this.Controls.Add(this.button_UnDigito);
            this.Controls.Add(this.button_Impares);
            this.Controls.Add(this.button_Pares);
            this.Controls.Add(this.button_G);
            this.Controls.Add(this.button_Sacar);
            this.Controls.Add(this.label_Resultado);
            this.Controls.Add(this.button_E);
            this.Controls.Add(this.button_B);
            this.Controls.Add(this.button_A);
            this.Controls.Add(this.button_C_D);
            this.Name = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label labelResultado;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button A;
        private System.Windows.Forms.Label Resultado;
        private System.Windows.Forms.Button B;
        private System.Windows.Forms.Button Limpiar;
        private System.Windows.Forms.Button E;
        private System.Windows.Forms.Button button_C_D;
        private System.Windows.Forms.Button button_B;
        private System.Windows.Forms.Button button_E;
        private System.Windows.Forms.Button button_A;
        private System.Windows.Forms.Label label_Resultado;
        private System.Windows.Forms.Button button_Sacar;
        private System.Windows.Forms.Button button_G;
        private System.Windows.Forms.Button button_Pares;
        private System.Windows.Forms.Button button_Impares;
        private System.Windows.Forms.Button button_UnDigito;
        private System.Windows.Forms.Button button_DosDigitos;
        private System.Windows.Forms.Button button_estan;
        private System.Windows.Forms.Button button_NoEstan;
        private System.Windows.Forms.Label label_Resultado2;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
    }
}

