﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _06_WindowsFormsApplicationClases
{
    public partial class Form1 : Form
    {
        private ListaNumeros L1;
        private ListaNumeros L2;




        public Form1()
        {
            InitializeComponent();
            L1 = new ListaNumeros();
            L2 = new ListaNumeros();

        }
        private void Limpiar_Click(object sender, EventArgs e)
        {
            label_Resultado.Text = ("Resultado: ");
            L1.Vaciar();
            label_Resultado2.Text = ("Resultado: ");
            L2.Vaciar();
        }

        private void A_Click(object sender, EventArgs e)
        {
            Random rnd = new Random();
            for (int i = 0; i < 10; i++)
            {
                int numero = rnd.Next(0, 100);
                L1.InsertarValor(numero);
                string Numero = (numero.ToString());
                label_Resultado.Text += (Numero + " ");
            }
        }

        private void B_Click(object sender, EventArgs e)
        {
            Random rnd = new Random();
            for (int i = 0; i < 15; i++)
            {
                int numero = rnd.Next(0, 100);
                L1.InsertarValor(numero);
                string Numero = (numero.ToString());
                label_Resultado2.Text += (Numero + " ");
            }
        }

        private void E_Click(object sender, EventArgs e)
        {
            int total = L1.NumeroElementos();
            label_Resultado.Text = ("Resultado: ");
            for (int cnt=total; cnt > 0; cnt--)
            {
                int numero = L1.Elemento(cnt);
                label_Resultado.Text += numero.ToString()+(" ");
            }
        }

        private void button_Sacar_Click(object sender, EventArgs e)
        {
            label_Resultado.Text = ("Resultado: ");
            int total = L1.NumeroElementos();

            for (int cnt = 0; cnt < total; cnt++)
            {
                int numero = L1.Sacar();
                label_Resultado.Text += numero.ToString() + (" ");
            }

        }

        private void button_G_Click(object sender, EventArgs e)
        {
            label_Resultado.Text = "Resultado: ";
            int[] aux = new int[100];

            for (int i = 0; i<10; i++)
            {
                aux[i] = L1.Sacar();
            }

            for (int cnt = 9; cnt>=0; cnt--)
            {
                label_Resultado.Text += aux[cnt] + " ";
            } 

        }

        private void button_Pares_Click(object sender, EventArgs e)
        {
            label_Resultado.Text = ("Resultado: ");
            int total = L1.NumeroElementos();

            for (int cnt = 0; cnt < total; cnt++)
            {
                int numero = L1.Sacar();
                if (numero %2 == 0)
                {
                    label_Resultado.Text += numero.ToString() + (" ");
                }
            }
        }

        private void button_Impares_Click(object sender, EventArgs e)
        {
            label_Resultado.Text = ("Resultado: ");
            int total = L1.NumeroElementos();

            for (int cnt = 0; cnt < total; cnt++)
            {
                int numero = L1.Sacar();
                if (numero % 2 != 0)
                {
                    label_Resultado.Text += numero.ToString() + (" ");
                }
            }
        }

        private void button_UnDigito_Click(object sender, EventArgs e)
        {
            label_Resultado.Text = ("Resultado: ");
            int total = L1.NumeroElementos();

            for (int cnt = 0; cnt < total; cnt++)
            {
                int numero = L1.Sacar();
                if (numero <9)
                {
                    label_Resultado.Text += numero.ToString() + (" ");
                }
            }
        }

        private void button_DosDigitos_Click(object sender, EventArgs e)
        {
            label_Resultado.Text = ("Resultado: ");
            int total = L1.NumeroElementos();

            for (int cnt = 0; cnt < total; cnt++)
            {
                int numero = L1.Sacar();
                if (numero >9)
                {
                    label_Resultado.Text += numero.ToString() + (" ");
                }
            }
        }


        private void button_estan_Click(object sender, EventArgs e)
        {
            label_Resultado.Text = ("Resultado: ");
            label_Resultado2.Text = ("");

            int[] lista1 = new int[100];
            int[] lista2 = new int[100];

            for (int cnt= 1; cnt <=10; cnt++)
            {
                lista1[cnt - 1] = L1.Elemento(cnt);
            }
            for (int cnt = 1; cnt <= 15; cnt++)
                lista2[cnt - 1] = L2.Elemento(cnt);

            for (int cnt = 0; cnt < 10; cnt++)
            {
                for( int cnt2 = 0; cnt2<15; cnt2++)
                {
                    if (lista1[cnt] == lista2[cnt2])
                        label_Resultado.Text += lista2[cnt2] + " ";
                }
            }
        }

        private void button_NoEstan_Click(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
            Random rnd = new Random();
            for (int i = 0; i < 10; i++)
            {
                int numero = rnd.Next(0, 100);
                L1.InsertarValor(numero);
                string Numero = (numero.ToString());
                label_Resultado.Text += (Numero + " ");
            }
            for (int i = 0; i < 15; i++)
            {
                int numero2 = rnd.Next(0, 100);
                string Numero2 = (numero2.ToString());
                L2.InsertarValor(numero2);
                label_Resultado2.Text += (Numero2 + " ");
            }

        }

        private void button7_Click(object sender, EventArgs e)
        {
            Fracciones f1 = new Fracciones(3);
            Fracciones f2 = new Fracciones(5, 4);

            Fracciones f3 = f1.multiplicar(f2);

            label_Resultado2.Text = f1.pinta() +" "+  f2.pinta() +" "+ f3.pinta();
        }
    }
}