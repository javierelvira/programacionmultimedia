﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06_WindowsFormsApplicationClases
{
    class Fracciones
    {
        private int numerador;
        private int denominador;
        
        public Fracciones(int num)
        {
            numerador = num;
            denominador = 1;
        }
        public Fracciones(int num, int denom)
        {
            numerador = num;
            denominador = denom;
        }

        
        public Fracciones multiplicar(Fracciones f2)
        {
            return (new Fracciones(numerador * f2.numerador, denominador * f2.denominador));
        }

        public string pinta()
        {
            return (""+numerador+"/"+denominador);
        }
    }

}
