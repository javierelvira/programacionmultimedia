﻿namespace _05_WindowsFormsApplicationClases
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.circulo1 = new System.Windows.Forms.Button();
            this.circulo2 = new System.Windows.Forms.Button();
            this.circulo3 = new System.Windows.Forms.Button();
            this.radio1 = new System.Windows.Forms.TextBox();
            this.color1 = new System.Windows.Forms.TextBox();
            this.radio2 = new System.Windows.Forms.TextBox();
            this.color2 = new System.Windows.Forms.TextBox();
            this.radio3 = new System.Windows.Forms.TextBox();
            this.color3 = new System.Windows.Forms.TextBox();
            this.labelResultado = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // circulo1
            // 
            this.circulo1.Location = new System.Drawing.Point(123, 77);
            this.circulo1.Name = "circulo1";
            this.circulo1.Size = new System.Drawing.Size(75, 23);
            this.circulo1.TabIndex = 0;
            this.circulo1.Text = "circulo1";
            this.circulo1.UseVisualStyleBackColor = true;
            this.circulo1.Click += new System.EventHandler(this.circulo1_Click);
            // 
            // circulo2
            // 
            this.circulo2.Location = new System.Drawing.Point(251, 77);
            this.circulo2.Name = "circulo2";
            this.circulo2.Size = new System.Drawing.Size(75, 23);
            this.circulo2.TabIndex = 1;
            this.circulo2.Text = "circulo2";
            this.circulo2.UseVisualStyleBackColor = true;
            // 
            // circulo3
            // 
            this.circulo3.Location = new System.Drawing.Point(399, 77);
            this.circulo3.Name = "circulo3";
            this.circulo3.Size = new System.Drawing.Size(75, 23);
            this.circulo3.TabIndex = 2;
            this.circulo3.Text = "circulo3";
            this.circulo3.UseVisualStyleBackColor = true;
            // 
            // radio1
            // 
            this.radio1.Location = new System.Drawing.Point(110, 124);
            this.radio1.Name = "radio1";
            this.radio1.Size = new System.Drawing.Size(100, 20);
            this.radio1.TabIndex = 3;
            // 
            // color1
            // 
            this.color1.Location = new System.Drawing.Point(110, 168);
            this.color1.Name = "color1";
            this.color1.Size = new System.Drawing.Size(100, 20);
            this.color1.TabIndex = 4;
            // 
            // radio2
            // 
            this.radio2.Location = new System.Drawing.Point(239, 124);
            this.radio2.Name = "radio2";
            this.radio2.Size = new System.Drawing.Size(100, 20);
            this.radio2.TabIndex = 5;
            // 
            // color2
            // 
            this.color2.Location = new System.Drawing.Point(239, 168);
            this.color2.Name = "color2";
            this.color2.Size = new System.Drawing.Size(100, 20);
            this.color2.TabIndex = 6;
            // 
            // radio3
            // 
            this.radio3.Location = new System.Drawing.Point(385, 124);
            this.radio3.Name = "radio3";
            this.radio3.Size = new System.Drawing.Size(100, 20);
            this.radio3.TabIndex = 7;
            // 
            // color3
            // 
            this.color3.Location = new System.Drawing.Point(385, 168);
            this.color3.Name = "color3";
            this.color3.Size = new System.Drawing.Size(100, 20);
            this.color3.TabIndex = 8;
            // 
            // labelResultado
            // 
            this.labelResultado.AutoSize = true;
            this.labelResultado.Location = new System.Drawing.Point(220, 229);
            this.labelResultado.Name = "labelResultado";
            this.labelResultado.Size = new System.Drawing.Size(79, 13);
            this.labelResultado.TabIndex = 9;
            this.labelResultado.Text = "El resultado es:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(625, 310);
            this.Controls.Add(this.labelResultado);
            this.Controls.Add(this.color3);
            this.Controls.Add(this.radio3);
            this.Controls.Add(this.color2);
            this.Controls.Add(this.radio2);
            this.Controls.Add(this.color1);
            this.Controls.Add(this.radio1);
            this.Controls.Add(this.circulo3);
            this.Controls.Add(this.circulo2);
            this.Controls.Add(this.circulo1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button circulo1;
        private System.Windows.Forms.Button circulo2;
        private System.Windows.Forms.Button circulo3;
        private System.Windows.Forms.TextBox radio1;
        private System.Windows.Forms.TextBox color1;
        private System.Windows.Forms.TextBox radio2;
        private System.Windows.Forms.TextBox color2;
        private System.Windows.Forms.TextBox radio3;
        private System.Windows.Forms.TextBox color3;
        private System.Windows.Forms.Label labelResultado;
    }
}

