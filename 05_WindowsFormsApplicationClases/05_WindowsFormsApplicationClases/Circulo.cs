﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_WindowsFormsApplicationClases
{
    class Circulo
    {
        private int radio;
        private string color;

        public Circulo(int r, string c)
        {
            radio = r;
            color = c;
        }

        public double Area()
        {
            return 3.14 * radio * radio;
        }
    }
}
