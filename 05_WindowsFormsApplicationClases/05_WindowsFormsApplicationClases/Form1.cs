﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _05_WindowsFormsApplicationClases
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void circulo1_Click(object sender, EventArgs e)
        {
            Circulo c1 = new Circulo(Int32.Parse(radio1.Text), color1.Text);
            double areaC1 = c1.Area();

            labelResultado.Text = "El area es " + areaC1;
        }
    }
}
