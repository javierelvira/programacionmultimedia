﻿namespace _03WindowsFormsIterativas
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonImprimir = new System.Windows.Forms.Button();
            this.textBoxNumero = new System.Windows.Forms.TextBox();
            this.labelResultado = new System.Windows.Forms.Label();
            this.buttonImprimirTabla = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonImprimir
            // 
            this.buttonImprimir.Location = new System.Drawing.Point(564, 92);
            this.buttonImprimir.Name = "buttonImprimir";
            this.buttonImprimir.Size = new System.Drawing.Size(207, 50);
            this.buttonImprimir.TabIndex = 0;
            this.buttonImprimir.Text = "Imprimir Números";
            this.buttonImprimir.UseVisualStyleBackColor = true;
            this.buttonImprimir.Click += new System.EventHandler(this.buttonImprimir_Click);
            // 
            // textBoxNumero
            // 
            this.textBoxNumero.Location = new System.Drawing.Point(312, 102);
            this.textBoxNumero.Name = "textBoxNumero";
            this.textBoxNumero.Size = new System.Drawing.Size(125, 32);
            this.textBoxNumero.TabIndex = 1;
            // 
            // labelResultado
            // 
            this.labelResultado.AutoSize = true;
            this.labelResultado.Location = new System.Drawing.Point(317, 175);
            this.labelResultado.Name = "labelResultado";
            this.labelResultado.Size = new System.Drawing.Size(0, 24);
            this.labelResultado.TabIndex = 2;
            this.labelResultado.Click += new System.EventHandler(this.labelResultado_Click);
            // 
            // buttonImprimirTabla
            // 
            this.buttonImprimirTabla.Location = new System.Drawing.Point(564, 162);
            this.buttonImprimirTabla.Name = "buttonImprimirTabla";
            this.buttonImprimirTabla.Size = new System.Drawing.Size(207, 50);
            this.buttonImprimirTabla.TabIndex = 0;
            this.buttonImprimirTabla.Text = "Imprimir Tabla";
            this.buttonImprimirTabla.UseVisualStyleBackColor = true;
            this.buttonImprimirTabla.Click += new System.EventHandler(this.buttonImprimirTabla_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Highlight;
            this.ClientSize = new System.Drawing.Size(1126, 604);
            this.Controls.Add(this.labelResultado);
            this.Controls.Add(this.textBoxNumero);
            this.Controls.Add(this.buttonImprimirTabla);
            this.Controls.Add(this.buttonImprimir);
            this.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "Form1";
            this.Text = "Sentencias Iterativas";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonImprimir;
        private System.Windows.Forms.TextBox textBoxNumero;
        private System.Windows.Forms.Label labelResultado;
        private System.Windows.Forms.Button buttonImprimirTabla;
    }
}

