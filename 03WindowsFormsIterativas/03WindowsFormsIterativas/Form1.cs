﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _03WindowsFormsIterativas
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void buttonImprimir_Click(object sender, EventArgs e)
        {
            int numero;

            numero = Int32.Parse(textBoxNumero.Text);
            for(int cnt = 1; cnt < numero; cnt++)
            {
                if (cnt != numero)
                    labelResultado.Text += (cnt + "-");
                else
                    labelResultado.Text += cnt;
            }
        }

        private void labelResultado_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void buttonImprimirTabla_Click(object sender, EventArgs e)
        {
            int[] tabla = { 11, 22, 33, 44, 55 };

            //for tradicional
            /*for (int cnt = 0; cnt < tabla.Length; cnt++)
            {
                labelResultado.Text += tabla[cnt]+ " ";
            }
            */
            //for solo para colecciones
            foreach(int numero in tabla)
            {
                labelResultado.Text += numero + " ";
            }
        }
    }
}
